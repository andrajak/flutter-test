import 'package:get/get.dart';
import '../modules/employee/detail/detail_binding.dart';
import '../modules/employee/detail/detail_page.dart';
import '../modules/employee/list/list_binding.dart';
import '../modules/employee/list/list_page.dart';
import '../modules/employee/new/new_binding.dart';
import '../modules/employee/new/new_page.dart';

class AppRoutes {
  static const employeeList = '/employeeList';
  static const employeeNew = '/employeeNew';
  static const employeeDetail = '/employeeDetail';
}

appRoutes() => [
      GetPage(
        name: '/employeeList',
        page: () => const EmployeeListPage(),
        binding: EmployeeListBinding(),
        transition: Transition.noTransition,
      ),
      GetPage(
        name: '/employeeNew',
        page: () => const EmployeeNewPage(),
        binding: EmployeeNewBinding(),
        transition: Transition.noTransition,
      ),
      GetPage(
        name: '/employeeDetail',
        page: () => const EmployeeDetailPage(),
        binding: EmployeeDetailBinding(),
        transition: Transition.noTransition,
      ),
    ];