import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum SnackBarType {
  error(Colors.red),
  info(Colors.blue),
  success(Colors.green);

  final Color color;
  const SnackBarType(this.color);
}

class CustomSnackBar {
  static void show({
      SnackBarType? type,
      String? title,
      required String message,}) {
    Get.snackbar(
      title ?? 'Error',
      message,
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: type != null ? type.color : SnackBarType.error.color,
      titleText: Text(
        title ?? 'Error',
        style: Theme.of(Get.context!).textTheme.titleLarge!.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.white),
      ),
      messageText: Text(
        message,
        style: Theme.of(Get.context!).textTheme.titleLarge!.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Colors.white),
      ),
      colorText: Colors.white,
      borderRadius: 8,
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(8),
      icon: const Icon(
        Icons.error_outline,
        size: 40,
        color: Colors.white,
      ),
      shouldIconPulse: true,
      duration: const Duration(seconds: 4),
    );
  }
}