import 'dart:convert';
import 'package:floor/floor.dart';

List<Employee> employeeFromJson(String str) =>
    List<Employee>.from(json.decode(str).map((x) => Employee.fromJson(x)));

String employeeToJson(List<Employee> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@entity
class Employee {
  @primaryKey
  final int? id;
  final String? name;
  final String? department;
  final String? address;
  final int? salary;
  final String? photoUrl;

  Employee({
    this.id,
    this.name,
    this.department,
    this.address,
    this.salary,
    this.photoUrl,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
    id: json["id"],
    name: json["name"],
    department: json["department"],
    address: json["address"],
    salary: json["salary"],
    photoUrl: json["photoUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "department": department,
    "address": address,
    "salary": salary,
    "photoUrl": photoUrl,
  };
}