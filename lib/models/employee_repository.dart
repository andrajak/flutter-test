import 'package:get_it/get_it.dart';
import 'package:mobilesoft_test/models/employee.dart';
import 'package:mobilesoft_test/models/employee_dao.dart';

class EmployeeRepository {

  late EmployeeDao _employeeDao;

  EmployeeRepository(){
    _employeeDao = GetIt.instance.get<EmployeeDao>();
  }

  Future<List<Employee>> findAllEmployees(){
    return _employeeDao.findAllEmployees();
  }

  Future<Employee?> findEmployeeById(int id){
    return _employeeDao.findEmployeeById(id);
  }

  Future<List<int>> insertEmployees(List<Employee> employees){
    return _employeeDao.insertEmployees(employees);
  }

  Future<int> insertEmployee(Employee employee){
    return _employeeDao.insertEmployee(employee);
  }

  Future<void> deleteEmployee(Employee employee){
    return _employeeDao.deleteEmployee(employee);
  }
}