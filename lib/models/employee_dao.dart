import 'package:floor/floor.dart';

import 'employee.dart';

@dao
abstract class EmployeeDao {
  @Query('SELECT * FROM Employee WHERE id = :id')
  Future<Employee?> findEmployeeById(int id);

  @Query('SELECT * FROM Employee')
  Future<List<Employee>> findAllEmployees();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertEmployee(Employee employee);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<List<int>> insertEmployees(List<Employee> employees);

  @update
  Future<void> updateEmployee(Employee employee);

  @update
  Future<void> updateEmployees(List<Employee> employee);

  @delete
  Future<void> deleteEmployee(Employee employee);

  @delete
  Future<void> deleteEmployees(List<Employee> employees);
}