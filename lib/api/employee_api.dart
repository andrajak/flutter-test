import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:mobilesoft_test/api/base_urls.dart';
import 'package:mobilesoft_test/models/employee.dart';
import '../widgets/custom_snack_bar.dart';

class EmployeeApi {
  final _dio = Dio(BaseOptions(baseUrl: BaseUrls.apiBaseUrl));

  Future<dynamic> getAllEmployees() async {
    try {
      final response = await _dio.get('/employees');
      if (response.statusCode == 200) {
        return response.data;
      } else {
        CustomSnackBar.show(message: 'Loading employee data failed!');
        return null;
      }
    } catch (e) {
      _handleGetEmployeeError(e);
    }
  }

  Future<dynamic> getEmployeeDetails(int employeeId) async {
    try {
      final response = await _dio.get('/employees/$employeeId');
      if (response.statusCode == 200) {
        return response.data;
      } else {
        CustomSnackBar.show(message: 'Loading employee data failed!');
        return null;
      }
    } catch (e) {
      _handleGetEmployeeError(e);
    }
  }

  void _handleGetEmployeeError(dynamic e) {
    if (e.response?.statusCode == 403) {
      CustomSnackBar.show(
          message: 'Loading employee data failed!', title: 'Error - Forbidden');
      return;
    } else if (e.response?.statusCode == 500) {
      CustomSnackBar.show(
          message: 'Employee data cannot be loaded, the server is down!',
          title: 'Internal Server Error');
      return;
    } else {
      CustomSnackBar.show(message: 'Loading employee data failed!');
    }
  }

  Future<dynamic> postEmployee(Employee employee, File? image) async {
    try {
      final formData = await _buildFormData(employee, image);
      final response = await _dio.post(
        '/employees',
        data: formData,
        options: Options(headers: {
          "Content-Type": "multipart/form-data; boundary=BOUNDARY",
        }),
      );
      if (response.statusCode == 200) {
        CustomSnackBar.show(
          type: SnackBarType.success,
          title: 'Done',
          message: 'A new employee has been successfully created!',
        );
        return response.data;
      } else {
        CustomSnackBar.show(message: 'A new employee has not been created!');
        return null;
      }
    } catch (e) {
      _handlePostEmployeeError(e);
    }
  }

  Future<FormData> _buildFormData(Employee employee, File? image) async {
    FormData formData;
    if (image != null) {
      String imageName = image.path.split('/').last;
      formData = FormData.fromMap({
        "json": jsonEncode(employee.toJson()),
        "photo": await MultipartFile.fromFile(image.path, filename: imageName),
      });
    } else {
      formData = FormData.fromMap({"json": jsonEncode(employee.toJson())});
    }
    return formData;
  }

  void _handlePostEmployeeError(dynamic e) {
    if (e.response?.statusCode == 403) {
      CustomSnackBar.show(
          message: 'The entered data is not valid!',
          title: 'Error - Forbidden');
    } else if (e.response?.statusCode == 500) {
      CustomSnackBar.show(
          message: 'A new employee cannot be created, the server is down!',
          title: 'Internal Server Error');
    } else {
      CustomSnackBar.show(message: 'A new employee has not been created!');
    }
  }
}
