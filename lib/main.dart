import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';

import 'database/database.dart';
import 'models/employee_dao.dart';
import 'models/employee_repository.dart';
import 'modules/employee/list/list_binding.dart';
import 'modules/employee/list/list_page.dart';
import 'routes/app_routes.dart';

Future<void> main() async {
  GetIt getIt = GetIt.instance;

  getIt.registerSingletonAsync<AppDatabase>(
          () async => $FloorAppDatabase.databaseBuilder('test80.db').build());

  getIt.registerSingletonWithDependencies<EmployeeDao>(() {
    return GetIt.instance.get<AppDatabase>().employeeDao;
  }, dependsOn: [AppDatabase]);

  getIt.registerSingletonWithDependencies<EmployeeRepository>(
          () => EmployeeRepository(),
      dependsOn: [AppDatabase, EmployeeDao]);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      smartManagement: SmartManagement.full,
        home: FutureBuilder(
            future: GetIt.instance.allReady(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if(snapshot.hasData){
                return const EmployeeListPage();
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            }),
      initialBinding: EmployeeListBinding(),
      getPages: appRoutes(),
    );
  }
}