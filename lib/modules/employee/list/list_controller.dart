import 'dart:async';
import 'dart:developer' as developer;
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:mobilesoft_test/models/employee.dart';
import 'package:mobilesoft_test/routes/app_routes.dart';

import '../../../api/employee_api.dart';
import '../../../models/employee_repository.dart';
import '../../../widgets/custom_snack_bar.dart';

class EmployeeListController extends GetxController {
  final EmployeeRepository _employeeRepository = EmployeeRepository();
  final EmployeeApi _employeeApi = EmployeeApi();
  RxList allEmployees = [].obs;
  RxBool isLoading = true.obs;

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();

  @override
  Future<void> onInit() async {
    super.onInit();
    await _initConnectivity();
    _fetchAllEmployeesRepository();
    _fetchAllEmployeesServer();
  }

  @override
  void onClose() {
    super.dispose();
  }

  Future<void> _initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    if (result == ConnectivityResult.none) {
      CustomSnackBar.show(
          type: SnackBarType.error,
          title: 'Internet connection error',
          message: 'The application must be connected to the Internet to work properly!');
      return;
    }

    _connectionStatus = result;
  }

  Future<void> _fetchAllEmployeesRepository() async {
    var employees = await _employeeRepository.findAllEmployees();
    if (employees.isNotEmpty) {
      allEmployees.assignAll(employees);
      isLoading.value = false;
      update();
    } else if (_connectionStatus == ConnectivityResult.none) {
      isLoading.value = false;
      update();
    }
  }

  void _fetchAllEmployeesServer() async {
    if (_connectionStatus == ConnectivityResult.none) {
      return;
    }
    var response = await _employeeApi.getAllEmployees();
    if (response != null) {
      for (var data in response) {
        Employee employee = Employee.fromJson(data);
        await _insertEmployee(employee);
      }
      isLoading.value = false;
      update();
    }
  }

  void insertEmployee(Employee employee) {
    _insertEmployee(employee);
    update();
  }

  Future<void> _insertEmployee(Employee employee) async {
    final existingEmployee =
    await _employeeRepository.findEmployeeById(employee.id!);

    if (existingEmployee != null) {
      // Handle the case when the employee ID already exists
      // In this specific case it's OK do nothing
    } else {
      try {
        allEmployees.add(employee);
        _employeeRepository.insertEmployee(employee);
      } catch (e) {
        CustomSnackBar.show(
            message: 'Insert a new employee to repository failed!');
      }
    }
  }

  void moveToNewEmployee() {
    if (_connectionStatus == ConnectivityResult.none) {
      CustomSnackBar.show(
          title: 'Internet connection error',
          message: 'Without an internet connection it is not possible to create new employees!');
      return;
    }
    Get.toNamed(AppRoutes.employeeNew);
  }

  void moveToEmployeeDetail(final int employeeId) async {
    var response = await _employeeApi.getEmployeeDetails(employeeId);
    if (response != null) {
      Employee employee = Employee.fromJson(response);
      Get.toNamed(AppRoutes.employeeDetail, arguments: {
        'id': employee.id,
        'name': employee.name ?? '',
        'department': employee.department ?? '',
        'salary': employee.salary ?? '',
        'address': employee.address ?? '',
        'photoUrl': employee.photoUrl ?? '',
      });
    }
  }
}
