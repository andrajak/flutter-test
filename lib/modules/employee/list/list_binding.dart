import 'package:get/get.dart';

import 'list_controller.dart';

class EmployeeListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EmployeeListController());
  }
}