import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../api/base_urls.dart';
import 'list_controller.dart';

class EmployeeListPage extends GetView<EmployeeListController> {
  const EmployeeListPage({super.key});

  @override
  Widget build(context) => Scaffold(
      appBar: AppBar(
        title: const Text(
          'Employees',
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => controller.moveToNewEmployee(),
            icon: const Icon(
              Icons.add,
              color: Colors.white,
              size: 50,
            ),
            padding: const EdgeInsets.fromLTRB(0, 0, 30, 0),
          )
        ],
      ),
      body: GetBuilder<EmployeeListController>(builder: (controller) {
        return controller.isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: controller.allEmployees.length * 2 - 1,
                  physics: const ScrollPhysics(),
                  itemBuilder: (context, index) {
                    if (index.isEven) {
                      final employeeIndex = index ~/ 2;
                      final employee = controller.allEmployees[employeeIndex];
                      return GestureDetector(
                        onTap: () =>
                            controller.moveToEmployeeDetail(employee.id),
                        child: Container(
                          padding: const EdgeInsets.fromLTRB(9, 6, 10, 2),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: SizedBox(
                                  height: Get.height * 0.14,
                                  child: Center(
                                    child: employee.photoUrl != null
                                        ? CachedNetworkImage(
                                            imageUrl:
                                                '${BaseUrls.photoBaseUrl}${employee.photoUrl}',
                                            placeholder: (context, url) =>
                                                const CircularProgressIndicator(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    const Icon(
                                              Icons.error,
                                              size: 42,
                                              color: Colors.red,
                                            ),
                                          )
                                        : const Placeholder(),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  child: ListTile(
                                      contentPadding: const EdgeInsets.fromLTRB(
                                          12, 0, 0, 30),
                                      title: Text(
                                        employee.name ?? "",
                                        style: const TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 6, 0, 0),
                                        child: Text(
                                          employee.department ?? "",
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    } else {
                      return const Divider(
                        color: Colors.black,
                        height: 10,
                        thickness:
                            3, // Adjust the thickness of the line as needed
                      );
                    }
                  },
                ),
              );
      }));
}
