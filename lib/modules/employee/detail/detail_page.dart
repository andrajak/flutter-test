import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilesoft_test/modules/employee/detail/widgets/recordParameter.dart';

import '../../../api/base_urls.dart';
import 'detail_controller.dart';

class EmployeeDetailPage extends GetView<EmployeeDetailController> {
  const EmployeeDetailPage({super.key});

  @override
  Widget build(context) => Scaffold(
      appBar: AppBar(
        title: Text(
          Get.arguments['name'],
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.all(2),
        child: Column(children: [
          Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: Get.height * 0.3,
                  child: Center(
                    child: Get.arguments['photoUrl'] != ""
                        ? CachedNetworkImage(
                            imageUrl:
                                '${BaseUrls.photoBaseUrl}${Get.arguments['photoUrl']}',
                            imageBuilder: (context, imageProvider) =>
                                Image(image: imageProvider),
                            placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                            errorWidget: (context, url, error) => const Icon(
                              Icons.error,
                              size: 52,
                              color: Colors.red,
                            ),
                          )
                        : const Placeholder(),
                  ),
                ),
              ),
            ],
          ),
          RecordParameter(parameterName: 'Department', value: Get.arguments['department']),
          RecordParameter(parameterName: 'Salary', value: Get.arguments['salary'] != ''
              ? '${Get.arguments['salary']} Kc' : ''),
          RecordParameter(parameterName: 'Address', value: Get.arguments['address']),
        ]),
      ));
}
