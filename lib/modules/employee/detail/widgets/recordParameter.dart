import 'package:flutter/material.dart';

class RecordParameter extends StatelessWidget {
  final String parameterName;
  final String value;

  const RecordParameter({
    super.key,
    required this.parameterName,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(12, 16, 0, 10),
            child: Text(
              parameterName,
              style: const TextStyle(fontSize: 22),
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(12, 16, 0, 10),
            child: Text(
              value,
              style: const TextStyle(fontSize: 22),
            ),
          ),
        ),
      ],
    );
  }
}
