import 'package:get/get.dart';

import 'detail_controller.dart';

class EmployeeDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EmployeeDetailController());
  }
}