import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'new_controller.dart';

class EmployeeNewPage extends GetView<EmployeeNewController> {
  const EmployeeNewPage({super.key});

  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(
          title: const Text(
            'New Employee',
          ),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: SingleChildScrollView(
            child: Form(
              key: controller.formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            _imagePickerBottomSheet(context);
                          },
                          child: SizedBox(
                            height: Get.height * 0.14,
                            child: Center(
                              child: Obx(() => controller.image.value == null
                                  ? const Icon(
                                      Icons.photo_camera,
                                      size: 40,
                                    )
                                  : Container(
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: FileImage(
                                            File(controller.image.value!.path),
                                          ),
                                        ),
                                      ),
                                    )),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Column(
                              children: [
                                TextFormField(
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    labelText: 'Name',
                                  ),
                                  keyboardType: TextInputType.text,
                                  controller: controller.nameEditingController,
                                  validator: (value) {
                                    return controller.validateName(value);
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                TextFormField(
                                  maxLines: 2,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    labelText: 'Address',
                                  ),
                                  keyboardType: TextInputType.text,
                                  controller:
                                      controller.addressEditingController,
                                  validator: (value) {
                                    return controller.validateAddress(value);
                                  },
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      labelText: 'Department',
                    ),
                    keyboardType: TextInputType.text,
                    controller: controller.departmentEditingController,
                    validator: (value) {
                      return controller.validateDepartment(value);
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      labelText: 'Salary',
                    ),
                    keyboardType: TextInputType.number,
                    controller: controller.salaryEditingController,
                    validator: (value) {
                      return controller.validateSalary(value);
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: context.width),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)))),
                      child: const Text(
                        'Create',
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      ),
                      onPressed: () {
                        controller.validateAndCreate();
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );

  void _imagePickerBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(16.0),
          ),
        ),
        builder: (BuildContext builder) {
          return SizedBox(
            height: Get.height * 0.15,
            child: Padding(
              padding: const EdgeInsets.all(0),
              child: Column(
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.pop(context);
                      controller.selectImageByGallery();
                    },
                    child: SizedBox(
                      height: Get.height * 0.075,
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.photo,
                              size: 30,
                            ),
                          ),
                          Text('Select a photo from Gallery'),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.pop(context);
                      controller.selectImageByCamera();
                    },
                    child: SizedBox(
                      height: Get.height * 0.075,
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.photo_camera,
                              size: 30,
                            ),
                          ),
                          Text('Take a photo with your Camera'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
