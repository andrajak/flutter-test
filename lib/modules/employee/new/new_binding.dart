import 'package:get/get.dart';

import '../list/list_controller.dart';
import 'new_controller.dart';

class EmployeeNewBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EmployeeNewController());
  }
}