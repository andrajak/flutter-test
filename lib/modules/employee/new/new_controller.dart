import 'dart:io';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../list/list_controller.dart';
import '../../../api/employee_api.dart';
import '../../../models/employee.dart';
import '../../../widgets/custom_snack_bar.dart';

class EmployeeNewController extends GetxController {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController nameEditingController = TextEditingController();
  final TextEditingController addressEditingController = TextEditingController();
  final TextEditingController departmentEditingController = TextEditingController();
  final TextEditingController salaryEditingController = TextEditingController();
  bool _isFormValid = false;

  final EmployeeApi _employeeApi = EmployeeApi();

  Rx<File?> image = Rx<File?>(null);
  final ImagePicker _picker = ImagePicker();

  @override
  void onClose() {
    super.onClose();
    nameEditingController.dispose();
    addressEditingController.dispose();
    departmentEditingController.dispose();
    salaryEditingController.dispose();
    image.close();
  }

  String? validateName(String? value) {
    if (value == null ||
        value.isEmpty ||
        RegExp(r'^[A-Za-z ]+$').hasMatch(value)) {
      return null;
    } else {
      return 'Provide valid name';
    }
  }

  String? validateDepartment(String? value) {
    if (value == null ||
        value.isEmpty ||
        RegExp(r'^[A-Za-z0-9_. ]+$').hasMatch(value)) {
      return null;
    } else {
      return 'Provide valid department';
    }
  }

  String? validateAddress(String? value) {
    if (value == null ||
        value.isEmpty ||
        RegExp(r'^[A-Za-z0-9_., ]+$').hasMatch(value)) {
      return null;
    } else {
      return 'Provide valid address';
    }
  }

  String? validateSalary(String? value) {
    if (value == null ||
        value.isEmpty ||
        RegExp(r'^[0-9.]+$').hasMatch(value)) {
      return null;
    } else {
      return 'Provide valid salary';
    }
  }

  void selectImageByGallery() async {
    final XFile? xFile = await _picker.pickImage(source: ImageSource.gallery);
    final File file = File(xFile!.path);
    image.value = file;
  }

  void selectImageByCamera() async {
    final XFile? xFile = await _picker.pickImage(source: ImageSource.camera);
    final File file = File(xFile!.path);
    image.value = file;
  }

  void validateAndCreate() async {
    _isFormValid = formKey.currentState!.validate();
    if (!_isFormValid) {
      CustomSnackBar.show(
          title: 'Error in validation',
          message: 'Provide correct values for a new employee');
    } else {
      final response = await _employeeApi.postEmployee(
          Employee(
              name: nameEditingController.text,
              department: departmentEditingController.text.isNotEmpty
                  ? departmentEditingController.text
                  : null,
              address: addressEditingController.text.isNotEmpty
                  ? addressEditingController.text
                  : null,
              salary: salaryEditingController.text.isNotEmpty
                  ? int.parse(salaryEditingController.text)
                  : null),
          image.value);

      if (response != null) {
        Get.find<EmployeeListController>()
            .insertEmployee(Employee.fromJson(response));
      }

      _resetFormFields();
      update();
    }
  }

  void _resetFormFields() {
    nameEditingController.clear();
    addressEditingController.clear();
    departmentEditingController.clear();
    salaryEditingController.clear();
    image.value = null;
  }
}
