# Flutter Test Project

Simple app to list all the employees from server and show them in the list. Individual records can be open as new page. You can also create new record and send it to the server.

There are three screens. Every screen contains AppBar.

Every record **can have** a photo, name, department, address, and salary. If there is no photo, show a placeholder instead.

## Basic Requirements

- App must run on both Android and iOS
- Use GetX for State, Route, and Dependency management
- REST API communication - either GetX or Dio
- For DB, the Floor package can be used
- Basic image processing - loading and scaling
- Errors can be displayed in the SnackBars

### List All Records

When the app is opened, it will load list of records from the local database. In the meanwhile, it downloads the data from the server, parses them and saves to the database. The scrolling of the list should be smooth all the time. If there is no data after the first start of the app, show a progress bar instead.

### Create a New Records

If you create a new record, there should be an option to select photo from the phoho library or directly from the camera.

The post should be multipart with JSON and image data in one request. The server will return a new object so you can store it in the database and update the list.
